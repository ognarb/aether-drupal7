<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<header id="navbar" role="banner" class="header clearfix">
  <nav class="navbar navbar-expand-lg">
    <?php if (!empty($site_name)): ?>
      <a class="kde-logo name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"></a>
      <span class="name navbar-brand nav-link" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></span>
    <?php endif; ?>
    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation" data-_extension-text-contrast="fg">
        <span class="navbar-toggler-icon"></span>
      </button>
    <?php endif; ?>
    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <?php print neverland_generate_menu('main-menu'); ?>
        <?php /*
        <?php if (!empty($primary_nav)): ?>
          <?php print render($primary_nav); ?>
        <?php endif; ?>
        <?php if (!empty($secondary_nav)): ?>
          <?php print render($secondary_nav); ?>
        <?php endif; ?>
        <?php if (!empty($page['navigation'])): ?>
          <?php print render($page['navigation']); ?>
        <?php endif; */ ?>
      </ul>
    </div>
    <?php endif; ?>
  </nav>
</header>

<div class="main-container <?php print $container_class; ?>">

  <section role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </section> <!-- /#page-header -->

  <div class="row mt-2 mb-2">

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3 card" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb;
      endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3 card" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

  </div>
</div>

<?php if (!empty($page['footer'])): ?>
  <footer id="kFooter" class="footer">
        <section id="kFooterIncome" class="container">
            <div id="kDonateForm">
                <div class="center">
                    <h3>Donate to KDE <a href="/community/donations/index.php#money" target="_blank">Why Donate?</a></h3>
                    <form action="https://www.paypal.com/en_US/cgi-bin/webscr" method="post" onsubmit="return amount.value >= 2 || window.confirm('Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?');">
                            <input type="hidden" name="cmd" value="_donations">
                            <input type="hidden" name="lc" value="GB">
                            <input type="hidden" name="item_name" value="Development and communication of KDE software">
                            <input type="hidden" name="custom" value="//kde.org//donation_box">
                            <input type="hidden" name="currency_code" value="EUR">
                            <input type="hidden" name="cbt" value="Return to kde.org">
                            <input type="hidden" name="return" value="https://kde.org/community/donations/thanks_paypal">
                            <input type="hidden" name="notify_url" value="https://kde.org/community/donations/notify.php">
                            <input type="hidden" name="business" value="kde-ev-paypal@kde.org">
                            <input type="text" name="amount" value="20.00" id="donateAmountField" data-_extension-text-contrast=""> €
                            <button type="submit" id="donateSubmit" data-_extension-text-contrast="">Donate via PayPal</button>
                    </form>
                    <a href="/community/donations/others" id="otherWaysDonate" target="_blank">Other ways to donate</a>
                </div>
            </div>
            <div id="kMetaStore">
                <div class="center">
                    <h3>Visit the KDE MetaStore</h3>
                    <p>Show your love for KDE! Purchase books, mugs, apparel, and more to support KDE.</p>
                    <a href="/stuff/metastore" class="button">Click here to browse</a>
                </div>
            </div>
        </section>
        
        
        <section id="kLinks" class="container">
            <div class="row">
                <nav class="col-sm">
                    <h3>Products</h3>
                    <a href="/plasma-desktop">Plasma</a>
                    <a href="/applications/">KDE Applications</a>
                    <a href="/products/frameworks/">KDE Frameworks</a>
                    <a href="https://plasma-mobile.org/overview/">Plasma Mobile</a>
                    <a href="https://neon.kde.org/">KDE neon</a>
                    <a href="https://wikitolearn.org/" target="_blank">WikiToLearn</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Develop</h3>
                    <a href="https://techbase.kde.org/">TechBase Wiki</a>
                    <a href="https://api.kde.org/">API Documentation</a>
                    <a href="https://doc.qt.io/" target="_blank">Qt Documentation</a>
                    <a href="https://inqlude.org/" target="_blank">Inqlude Documentation</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>News &amp; Press</h3>
                    <a href="/announcements/">Announcements</a>
                    <a href="https://dot.kde.org/">KDE.news</a>
                    <a href="https://planetkde.org/">Planet KDE</a>
                    <a href="https://www.kde.org/screenshots">Screenshots</a>
                    <a href="https://www.kde.org/contact/">Press Contact</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Resources</h3>
                    <a href="https://community.kde.org/Main_Page">Community Wiki</a>
                    <a href="https://userbase.kde.org/">UserBase Wiki</a>
                    <a href="/stuff/">Miscellaneous Stuff</a>
                    <a href="/support/">Support</a>
                    <a href="/support/international.php">International Websites</a>
                    <a href="/download/">Download KDE Software</a>
                    <a href="/code-of-conduct/">Code of Conduct</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Destinations</h3>
                    <a href="https://store.kde.org/">KDE Store</a>
                    <a href="https://ev.kde.org/">KDE e.V.</a>
                    <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>
                    <a href="https://timeline.kde.org/">KDE Timeline</a>
                </nav>
            </div>
        </section>
    
        <section id="kSocial" class="container kSocialLinks">
            <!-- <nav style="position: absolute; left: 15px;">
                Browsing in English (Languages coming soon)
            </nav> -->
        
            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="me nofollow">Share on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
            <a class="shareReddit" href="https://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
            <a class="sharePeerTube" href="https://peertube.mastodon.host/accounts/kde/videos" rel="nofollow">Share on PeerTube</a>
        </section>
    
        <section id="kLegal" class="container">
            <div class="row">
                <small class="col-4">
                    Maintained by <a href="mailto:kde-webmaster@kde.org">KDE Webmasters</a>
                </small>
                <small class="col-8" style="text-align: right;">
                    KDE<sup>®</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>®</sup> logo</a> are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
                    <a href="https://www.kde.org/community/whatiskde/impressum">Legal</a>
                </small>
            </div>
        </section>
        <?php print render($page['footer']); ?>
    </footer>
<?php endif; ?>
