<?php

/**
 * @file
 * Stub file for bootstrap_menu_tree() and suggestion(s).
 */

/**
 * Returns HTML for a wrapper for a menu sub-tree.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see template_preprocess_menu_tree()
 * @see theme_menu_tree()
 *
 * @ingroup theme_functions
 */
function bootstrap_menu_tree(array &$variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the primary menu links.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 */
function bootstrap_menu_tree__primary(array &$variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the secondary menu links.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 */
function bootstrap_menu_tree__secondary(array &$variables) {
  return '<ul class="menu nav navbar-nav secondary">' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_tree() for book module.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 */
function bootstrap_menu_tree__book_toc(array &$variables) {
  $output = '<div class="book-toc btn-group pull-right">';
  $output .= '  <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">';
  $output .= t('!icon Outline !caret', array(
    '!icon' => _bootstrap_icon('list'),
    '!caret' => '<span class="caret"></span>',
  ));
  $output .= '</button>';
  $output .= '<ul class="dropdown-menu" role="menu">' . $variables['tree'] . '</ul>';
  $output .= '</div>';
  return $output;
}

/**
 * Overrides theme_menu_tree() for book module.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   The constructed HTML.
 */
function bootstrap_menu_tree__book_toc__sub_menu(array &$variables) {
  return '<ul class="dropdown-menu" role="menu">' . $variables['tree'] . '</ul>';
}

/**
 * Generates the link menus
 */
function neverland_generate_menu($menu_type) {
    if ($menu_type == 'header') {
        $output = '';
        $count = 0;

        // Fetch the navigation links for the menu
        $links = menu_navigation_links('menu-neverland-header');

        // Build menu items
        foreach ($links as $link) {
            if (!empty($link['attributes']['title'])) {
                $output .= '<li><a href="' . url($link['href']) . '">';
                $output .= '<h2>' . $link['title'];
                $output .= '<small>' . $link['attributes']['title'] . '</small>';
                $output .= '</h2>';
                $output .= '</a></li>';
            }

            // We allow max. 3 links in header
            if (++$count == 3) {
                break;
            }
        }

        return $output;
    }
    else if ($menu_type == 'main-menu') {
        // We fetch the entire menu tree as we want submenus as well
        $menu = neverland_get_main_menu_name();
        $tree = menu_tree_all_data($menu);

        // We hand over the data to the tree parser. This will call itself
        // recirsively for each child item
        return neverland_generate_menu_tree($tree);
    }
}

/**
 * Resolves a menu name based on category settings
 */
function neverland_get_main_menu_name() {
    $menu = 'main-menu';

    if (theme_get_setting('neverland_category_menus')) {
        $base = rtrim(base_path(), '/');
        $uri = str_replace($base, '', request_uri());
        $cat = explode('/', $uri);

        if (isset($cat[1]) && !empty($cat[1])) {
            $menu = "menu-category-{$cat[1]}";
        }
    }

    return $menu;
}

/**
 * Generates menu items from the menu tree
 *
 * This function recursively calls itself if there are more child items
 */
function neverland_generate_menu_tree($tree, $first_child = true) {
    $output = '';

    // Traverse through the menu tree and resolve the structure
    foreach ($tree as $item) {
        $link = $item['link'];

        if (!empty($link['options']['attributes']['title'])) {
            $icon = '<i class="' . $link['options']['attributes']['title'] . '"></i>&nbsp;';
        }
        else {
            $icon = '<i class="icon-globe"></i>&nbsp;';
        }

        if (count($item['below']) == 0) {
            $output .= '<li>';
            $output .= '<a href="' . url($link['link_path']) . '">' . $icon . $link['link_title'] . '</a>';
            $output .= '</li>';
        }
        else {
            if ($first_child) {
                $output .= '<li class="dropdown">';
                $output .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                $output .= $icon . $link['link_title'];
                $output .= '&nbsp;<b class="caret"></b>';
                $output .= '</a>';
                $output .= '<ul class="dropdown-menu">';
                $output .= neverland_generate_menu_tree($item['below'], false);
                $output .= '</ul>';
                $output .= '</li>';
            }
            else {
                $output .= '<li class="dropdown-submenu">';
                $output .= '<a href="#" tabindex="-1">';
                $output .= $icon . $link['link_title'];
                $output .= '</a>';
                $output .= '<ul class="dropdown-menu">';
                $output .= neverland_generate_menu_tree($item['below'], false);
                $output .= '</ul>';
                $output .= '</li>';
            }
        }
    }

    return $output;
}
